package com.puravida.asciidoctor.barcode;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.Test;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class BarCodeBlockTests {

    @Test
    public void code128() {
        Map<String,Object> attributes = new HashMap<String,Object>();

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);
        options.setToDir(new File("build").getAbsolutePath());
        options.setOption("mkdirs", "true");
        options.setOption("imagesoutdir", "build");
        options.setToFile("code128.html");

        options.setAttributes(attributes);
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        asciidoctor.convert(String.format(
                "= My document\n\n" +
                        "preambulo \n"+
                        "----\n"+
                        "barcode::codabar[123456]\n"+
                        "----\n"+
                        "footer \n"+
                        ""), options);
    }

    @Test
    public void qrcode() {
        Map<String,Object> attributes = new HashMap<String,Object>();

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);
        options.setToDir(new File("build").getAbsolutePath());
        options.setOption("mkdirs", "true");
        options.setOption("imagesoutdir", "build");
        options.setToFile("qrcode.html");

        options.setAttributes(attributes);
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        asciidoctor.convert(String.format(
                "= My document\n\n" +
                        "preambulo \n"+
                        "----\n"+
                        "barcode::qrcode[http://puravida-software.com,300,300,FFAABB00,00FF9900]\n"+
                        "----\n"+
                        "footer \n"+
                        ""), options);
    }


}
