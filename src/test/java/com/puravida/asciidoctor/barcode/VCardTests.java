package com.puravida.asciidoctor.barcode;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.Test;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class VCardTests {

    @Test
    public void vcard() {
        Map<String,Object> attributes = new HashMap<String,Object>();

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);
        options.setToDir(new File("build").getAbsolutePath());
        options.setOption("mkdirs", "true");
        options.setOption("imagesoutdir", "build");
        options.setToFile("vcard.html");

        options.setAttributes(attributes);
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        asciidoctor.convert(String.format(
                "= My document\n\n" +
                        "preambulo \n"+
                        "[vcard,3.0,300,300]\n"+
                        "----\n"+

                        "N:Gump;Forrest;;Mr.;\n"+
                        "FN:Forrest Gump\n"+
                        "ORG:Bubba Gump Shrimp Co.\n"+
                        "TITLE:Shrimp Man\n"+
                        "PHOTO;VALUE=URI;TYPE=GIF:http://www.example.com/dir_photos/my_photo.gif\n"+
                        "TEL;TYPE=WORK,VOICE:(111) 555-1212\n"+
                        "TEL;TYPE=HOME,VOICE:(404) 555-1212\n"+
                        "ADR;TYPE=WORK,PREF:;;100 Waters Edge;Baytown;LA;30314;United States of America\n"+
                        "LABEL;TYPE=WORK,PREF:100 Waters Edge\\nBaytown\\, LA 30314\\nUnited States of America\n"+
                        "ADR;TYPE=HOME:;;42 Plantation St.;Baytown;LA;30314;United States of America\n"+
                        "LABEL;TYPE=HOME:42 Plantation St.\\nBaytown\\, LA 30314\\nUnited States of America\n"+
                        "EMAIL:forrestgump@example.com\n"+

                        "----\n"+
                        "footer \n"+
                        ""), options);
    }


}
