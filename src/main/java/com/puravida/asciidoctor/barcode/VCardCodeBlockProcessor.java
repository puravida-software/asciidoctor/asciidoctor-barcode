package com.puravida.asciidoctor.barcode;

import org.asciidoctor.ast.ContentModel;
import org.asciidoctor.ast.ContentNode;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.*;

import java.io.File;
import java.util.*;

@Name("vcard")
@Contexts({Contexts.LISTING})
@PositionalAttributes({"url","width","height"})
public class VCardCodeBlockProcessor extends BlockProcessor {

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {
        try {
            List<String> list = reader.readLines();

            String vcard = buildVCard(list, (String) attributes.get("version"));
            String width = (String) attributes.get("width");
            String height = (String) attributes.get("height");
            int iwidth = Integer.parseInt(width != null ? width : "300");
            int iheight = Integer.parseInt(height != null ? height : "300");

            String scolor = (String) attributes.get("color");
            String sbackg = (String) attributes.get("background");
            if (scolor == null) {
                scolor = "FF000000";
            }
            if (sbackg == null) {
                sbackg = "FFFFFFFF";
            }

            File fImageDir = ImgDirUtil.imageDir(parent);

            File fImage = ZxingMethods.generateImage("qrcode", fImageDir, vcard, iwidth, iheight, scolor, sbackg);
            if (fImage == null)
                return null;

            attributes.put("target", ImgDirUtil.imagePath(parent, fImage));
            attributes.put("alt", "vcard");
            attributes.put("width", width);
            attributes.put("height", height);

            return createBlock(parent, "image", "", attributes);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    String buildVCard(List<String>list, String version){
        StringBuffer sb = new StringBuffer();
        sb.append("BEGIN:VCARD\n");
        sb.append("VERSION:").append(version!=null?version:"3.0").append("\n");
        for(String s:list){
            sb.append(s).append("\n");
        }
        sb.append("END:VCARD\n");
        return sb.toString();
    }
}
