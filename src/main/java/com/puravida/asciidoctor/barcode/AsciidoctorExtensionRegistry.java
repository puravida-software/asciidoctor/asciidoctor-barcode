package com.puravida.asciidoctor.barcode;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;


public class AsciidoctorExtensionRegistry implements ExtensionRegistry {

    @Override
    public void register(Asciidoctor asciidoctor) {

        asciidoctor.javaExtensionRegistry().inlineMacro(BarCodeInlineMacroProcessor.class);

        asciidoctor.javaExtensionRegistry().blockMacro(BarCodeMacroProcessor.class);

        asciidoctor.javaExtensionRegistry().block(VCardCodeBlockProcessor.class);
    }
}
