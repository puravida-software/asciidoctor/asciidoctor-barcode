package com.puravida.asciidoctor.barcode;

import org.asciidoctor.ast.ContentNode;

import java.io.File;
import java.nio.file.Files;
import java.util.Map;

public class BarcodeProcessor {

    public File generateBarCode(ContentNode parent,
                                String target,
                                Map<String, Object> attributes,
                                Map<String,Object> attr){
        try {
            String url = (String) attributes.get("url");
            if (url == null)
                return null;

            String width = (String) attributes.get("width");
            String height = (String) attributes.get("height");
            int iwidth = Integer.parseInt(width != null ? width : "150");
            int iheight = Integer.parseInt(height != null ? height : "80");

            String scolor = (String) attributes.get("color");
            String sbackg = (String) attributes.get("background");
            if ("qrcode".equals(target)) {
                String backimg = (String) attributes.get("backimg");
                if (backimg != null) {
                    sbackg = sbackg == null ? "00000000" : sbackg;
                }
                String stamp = (String)attributes.get("stamp");
                if( stamp !=null){
                    sbackg = sbackg == null ? "00000000" : sbackg;
                }
            }
            if (scolor == null) {
                scolor = "FF000000";
            }
            if (sbackg == null) {
                sbackg = "FFFFFFFF";
            }

            File fImageDir = ImgDirUtil.imageDir(parent);

            File fImage = ZxingMethods.generateImage(target, fImageDir, url, iwidth, iheight, scolor, sbackg);
            if (fImage == null)
                return null;

            if ("qrcode".equals(target)) {
                File docdir = new File((String)parent.getDocument().getAttribute("docdir"));
                File imgdir = parent.getDocument().getAttribute("imagedir") != null ?
                        new File(docdir, (String)parent.getDocument().getAttribute("imagedir"))
                        :
                        new File(docdir, "images");

                String backimg = (String) attributes.get("backimg");
                if (backimg != null) {
                    File backImg = new File(imgdir, backimg);
                    byte[] bytes = ZxingMethods.mixImages(backImg, fImage, iwidth, iheight);
                    Files.write(fImage.toPath(), bytes);
                }
                String stamp = (String)attributes.get("stamp");
                if( stamp !=null){
                    File stampImg = new File(imgdir, stamp);
                    byte[] bytes = ZxingMethods.stampLogo(fImage, stampImg);
                    Files.write(fImage.toPath(), bytes);
                }
            }

            attr.put("target", ImgDirUtil.imagePath(parent, fImage));
            attr.put("alt", url);
            attr.put("width", width);
            attr.put("height", height);

            return fImage;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
