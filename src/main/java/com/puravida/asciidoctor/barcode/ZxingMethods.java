package com.puravida.asciidoctor.barcode;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.util.Date;

public class ZxingMethods {

    protected static String generateFileName(String target, String txt){
        String ret = "barcode-";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update((target+"-"+txt).getBytes());
            byte[] hashInBytes = md.digest((target+"-"+txt).getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
            ret += sb.toString();
        }catch (Exception e){
            ret = ""+new Date().getTime();
        }
        return ret+=".png";
    }


    protected static File generateImage(String target,
                                        File fImageDir, String txt, int width, int height) throws Exception {
        return generateImage(target, fImageDir, txt, width, height, "FF000000", "FFFFFFFF");
    }

    protected static File generateImage(String target, File fImageDir, String txt, int iwidth,
                                        int iheight, String color, String backColor) throws Exception{

        File fImage = new File(fImageDir, generateFileName(target, txt) );

        BarcodeFormat format = translateFormat(target);
        if(format == null){
            return null;
        }

        generateBarCodeImage(format, txt, iwidth, iheight, fImage.getAbsolutePath(),
                hexToIntColor(color), hexToIntColor(backColor));
        return fImage;
    }

    protected static void generateBarCodeImage(BarcodeFormat format, String text,
                                               int width, int height, String filePath,
                                               int color, int background)
            throws WriterException, IOException {
        BitMatrix bitMatrix =  new MultiFormatWriter().encode(text, format, width, height, null);
        Path path = FileSystems.getDefault().getPath(filePath);
        MatrixToImageConfig conf = new MatrixToImageConfig( color, background);
        MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path, conf);
    }

    protected static BarcodeFormat translateFormat(String format){
        switch( format ) {
            case "aztec":
                return BarcodeFormat.AZTEC;
            case "codabar":
                return BarcodeFormat.CODABAR;
            case "code39":
                return BarcodeFormat.CODE_39;
            case "code93":
                return BarcodeFormat.CODE_93;
            case "code128":
                return BarcodeFormat.CODE_128;
            case "ean8":
                return BarcodeFormat.EAN_8;
            case "ean13":
                return BarcodeFormat.EAN_13;
            case "itf":
                return BarcodeFormat.ITF;
            case "upca":
                return BarcodeFormat.UPC_A;
            case "upce":
                return BarcodeFormat.UPC_E;
            case "qrcode":
                return BarcodeFormat.QR_CODE;
            case "dmatrix":
                return BarcodeFormat.DATA_MATRIX;
            case "pdf417":
                return BarcodeFormat.PDF_417;
            default:
                return null;
        }
    }

    public static int hexToIntColor(String hex){
        int Alpha = Integer.valueOf(hex.substring(0, 2), 16);
        int Red = Integer.valueOf(hex.substring(2, 4), 16);
        int Green = Integer.valueOf(hex.substring(4, 6), 16);
        int Blue = Integer.valueOf(hex.substring(6, 8), 16);
        Alpha = (Alpha << 24) & 0xFF000000;
        Red = (Red << 16) & 0x00FF0000;
        Green = (Green << 8) & 0x0000FF00;
        Blue = Blue & 0x000000FF;
        return Alpha | Red | Green | Blue;
    }

    public static byte[] mixImages(File back, File front, int width, int height) throws IOException{
        BufferedImage backImage = ImageIO.read(back);
        BufferedImage frontImage = ImageIO.read(front);

        BufferedImage resizedBack = new BufferedImage(width, height, backImage.getType());
        BufferedImage resizedFront = new BufferedImage(width, height, frontImage.getType());

        Graphics2D gBack = resizedBack.createGraphics();
        gBack.drawImage(backImage, 0, 0, width, height, null);

        Graphics2D gFront = resizedFront.createGraphics();
        gFront.drawImage(frontImage, 0, 0, width, height, null);

        BufferedImage combined = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g = (Graphics2D) combined.getGraphics();
        g.drawImage(resizedBack,0,0,null);
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
        g.drawImage(resizedFront,0,0,null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(combined, "png", baos);
        return baos.toByteArray();
    }

    public static byte[] stampLogo(File qr, File logo) throws IOException{
        BufferedImage qrImage = ImageIO.read(qr);
        BufferedImage logoImage = ImageIO.read(logo);

        BufferedImage logoResized = new BufferedImage(qrImage.getWidth()/6, qrImage.getHeight()/6, logoImage.getType());
        Graphics2D gLogo = logoResized.createGraphics();
        gLogo.drawImage(logoImage, 0, 0, logoResized.getWidth(), logoResized.getHeight(), null);

        int deltaHeight = qrImage.getHeight() - logoResized.getHeight();
        int deltaWidth = qrImage.getWidth() - logoResized.getWidth();

        BufferedImage combined = new BufferedImage(qrImage.getWidth(), qrImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) combined.getGraphics();

        g.drawImage(qrImage, 0, 0, null);
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
        g.drawImage(logoResized, (int) Math.round(deltaWidth / 2), (int) Math.round(deltaHeight / 2), null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(combined, "png", baos);
        return baos.toByteArray();
    }

}
