package com.puravida.asciidoctor.barcode;

import org.asciidoctor.ast.ContentModel;
import org.asciidoctor.ast.ContentNode;
import org.asciidoctor.extension.*;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Name("barcode")
@Format(FormatType.LONG)
@PositionalAttributes({"url","width","height"})
public class BarCodeInlineMacroProcessor extends InlineMacroProcessor {

    @Override
    public Object process(ContentNode parent, String target, Map<String, Object> attributes)  {

        Map<String,Object> attr = new HashMap<>();
        BarcodeProcessor barcodeProcessor = new BarcodeProcessor();
        File output = barcodeProcessor.generateBarCode(parent,target,attributes, attr);
        if( output == null)
            return null;

        Map<String, Object> options = new HashMap<String, Object>();
        options.put("type", "image");
        options.put("target", attr.get("target"));

        return createPhraseNode(parent, "image", (String)null, attr, options);
    }
}
