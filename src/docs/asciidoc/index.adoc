= Barcodes
:imagesdir: images

Barcode es una extensión que te permite generar códigos de barra y QRCode de una forma
fácil.

== Dependencies

Lo primero que hay que hacer es incluir la dependencia a la extensión en tu proyecto:

[#barcode-dependencies,source,subs="attributes"]
----
plugins {
    id 'org.asciidoctor.jvm.convert' version '2.2.0'
}


repositories {
    jcenter()
}

configurations {
    asciidoctorExt
}

dependencies {
    asciidoctorExt "com.puravida-software.asciidoctor:asciidoctor-barcode:${puravidaVersion}"
}

asciidoctorj{
    version '2.0.0'
}

asciidoctor {
    configurations 'asciidoctorExt'
}

----

== Macro

Para generar un codigo de barras debes usar la macro `barcode`:

[source]
----
barcode:code128[ código_de_producto, width, height] //<1>
----
<1> `code128` es uno de los formatos de código de barras disponibles
<1> el código del producto
<1> ancho y alto opcional (automatico por defecto)


Por ejemplo, el siguiente párrafo:

.ImagesDir {imagesdir}
[source]
----
Producto "Tutorial basico de Asciidoctor", ref: 123456

barcode:code128[1234567890123]
----

genera el siguiente resultado:

====
Producto "Tutorial basico de Asciidoctor", ref: 123456

barcode:code128[1234567890123]
====

== Lista de Codes

A continuación se indican los diferentes `codes` soportados con un ejemplo y su resultado

.Códigos 1D
|===
| code | ejemplo | resultado

| aztec | pass:[barcode:aztec[123456,color=AAAAAA00]] | barcode:aztec[123456,color=AAAAAA00]

| codabar | pass:[barcode:codabar[123456]] | barcode:codabar[123456]

| code39 | pass:[barcode:code39[123456]] | barcode:code39[123456]

| code93 | pass:[barcode:code93[123456]]| barcode:code93[123456]

| code128 | pass:[barcode:code128[123456]]| barcode:code128[123456]

| ean8 | pass:[barcode:ean8[12345678]]| barcode:ean8[12345678]

| ean13 | pass:[barcode:ean13[0000642109529]] | barcode:ean13[0000642109529]

| itf | pass:[barcode:itf[123456]] | barcode:itf[123456]

| upca | pass:[barcode:upca[012345678905]] | barcode:upca[012345678905]

| upce | pass:[barcode:upce[01240136]] | barcode:upce[01240136]

|===

.Códigos 2D
|===
| code | ejemplo | resultado

| dmatrix
| pass:[barcode:dmatrix[This is an example of\nPuraVida\nAssciidoctor Codebar\nExtension,100,100,color=AAAAAA00]]
| barcode:dmatrix[This is an example of\nPuraVida\nAssciidoctor Codebar\nExtension,100,100,color=AAAAAA00]

| pdf417 | pass:[barcode:pdf417[123456,color=FFBB8800,background=00FF9900]] | barcode:pdf417[123456,color=FFBB8800,background=00FF9900]

| qrcode
| pass:[barcode:qrcode[https://puravida-software.gitlab.io/asciidoctor-extensions/#barcodes,100,100,color=FFAABB00,background=00FF9900]]
| barcode:qrcode[https://puravida-software.gitlab.io/asciidoctor-extensions/#barcodes,100,100,color=FFAABB00,background=00FF9900]

|===

== Inline y Block

Todos los códigos anteriores pueden ser usados tanto como macros `inline` como macros `block`:

[source]
----
Apunte su lector de código de barras sobre barcode:codabar[123456] la imagen anterior

Apunte su lector de código de barras sobre la siguiente imagen

barcode::codabar[123456]

----

.Resultado
====
Apunte su lector de código de barras sobre barcode:codabar[123456] la imagen anterior

Apunte su lector de código de barras sobre la siguiente imagen

barcode::codabar[123456,color=FFAABB00,background=00FF9900]

====

== QRCodes personalizados

Además de poder especificar los colores para los códigos, en el caso de QRCode podremos incluir una imagen
bien sea como background o como un pequeño icono en el medio, e incluso ambas a la vez:

pass:[barcode::qrcode[https://puravida-software.gitlab.io/asciidoctor-extensions/#qrcodes-personalizados,300,300,backimg=logo.png]]

barcode::qrcode[https://puravida-software.gitlab.io/asciidoctor-extensions/#qrcodes-personalizados,300,300,backimg=logo.png]


pass:[barcode::qrcode[https://puravida-software.gitlab.io/asciidoctor-extensions/#,300,300,stamp=logo.jpg]]

barcode::qrcode[https://puravida-software.gitlab.io/asciidoctor-extensions/#,300,300,stamp=logo.jpg]

pass:[barcode::qrcode[https://puravida-software.gitlab.io/asciidoctor-extensions/,600,600,backimg=logo.jpg,stamp=logo_resized.jpg]]

barcode::qrcode[https://puravida-software.gitlab.io/asciidoctor-extensions/,600,600,backimg=logo.jpg,stamp=logo_resized.jpg]


== VCard (Tarjeta de Contacto)

Puedes generar un código QR con los datos de contacto de una persona utilizando un bloque `vcard`

[source]
----
    [vcard, version, widht, height ] //<1>
    ----
    N:Gump;Forrest;;Mr.;        //<2>
    FN:Forrest Gump
    ORG:Bubba Gump Shrimp Co.
    TITLE:Shrimp Man
    PHOTO;VALUE=URI;TYPE=GIF:http://www.example.com/dir_photos/my_photo.gif
    TEL;TYPE=WORK,VOICE:(111) 555-1212
    TEL;TYPE=HOME,VOICE:(404) 555-1212
    ADR;TYPE=WORK,PREF:;;100 Waters Edge;Baytown;LA;30314;United States of America
    LABEL;TYPE=WORK,PREF:100 Waters Edge\nBaytown\, LA 30314\nUnited States of America
    ADR;TYPE=HOME:;;42 Plantation St.;Baytown;LA;30314;United States of America
    LABEL;TYPE=HOME:42 Plantation St.\nBaytown\, LA 30314\nUnited States of America
    EMAIL:forrestgump@example.com
    ----
----
<1> version por defecto es 3.0
<1> ancho y alto por defecto 300x300
<2> indica tantos campos como quieras, en el orden que quieras

.Forrest Gump VCard , version 3.0
[vcard, 3.0, 300,300,background=2200FF00,color=FF000000]
----
N:Gump;Forrest;;Mr.;
FN:Forrest Gump
ORG:Bubba Gump Shrimp Co.
TITLE:Shrimp Man
PHOTO;VALUE=URI;TYPE=GIF:http://www.example.com/dir_photos/my_photo.gif
TEL;TYPE=WORK,VOICE:(111) 555-1212
TEL;TYPE=HOME,VOICE:(404) 555-1212
ADR;TYPE=WORK,PREF:;;100 Waters Edge;Baytown;LA;30314;United States of America
LABEL;TYPE=WORK,PREF:100 Waters Edge\nBaytown\, LA 30314\nUnited States of America
ADR;TYPE=HOME:;;42 Plantation St.;Baytown;LA;30314;United States of America
LABEL;TYPE=HOME:42 Plantation St.\nBaytown\, LA 30314\nUnited States of America
EMAIL:forrestgump@example.com
----
